
import React from "react";
import ReactDOM from "react-dom";
import App from "./quiz/App";

ReactDOM.render(<App/>, document.getElementById("app"));