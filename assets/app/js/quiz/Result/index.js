import './style.scss';
import React, { Component } from 'react';


export default class Result extends Component {
    constructor(props){
        super(props);
    }

    state = this.props.result;

    componentDidMount(){
        const score = Math.round(this.state.result.score);

        let message = {
          text: '',
          level: ''
        };

        if(score <= 20){
            message.level = 'Beginner-Elementary'
        }
        else if(score > 20 && score <= 40){
            message.level = 'Pre-Intermediate'
        }
        else if(score > 40 && score <= 60){
            message.level = 'Intermediate'
        }
        else if(score > 60 && score <= 80){
            message.level = 'Upper-Intermediate'
        }
        else if(score > 80 && score <= 99){
            message.level = 'Advanced'
        } else {
            message.level = 'ідеальний'
        }

        this.setState({
                result: {
                    score: Math.round(this.state.result.score),
                    message: message.level
                }
            }
        );
    }
    render(){
        return(
            <div className={'result-container'}>
                <p>
                    Гарна робота! Ви набрали <b>{this.state.result.score}%</b> правильних відповідей!
                </p>
                <p>
                    Ваш приблизний рівень англійської: <b>{this.state.result.message}</b>!
                </p>
                <button className='button blue' onClick={this.props.removeResult}>Спробувати ще раз</button>

            </div>
        )
    }

}
