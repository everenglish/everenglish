import './style.scss';
import React from 'react';
import Answer from "../Answer";

export default class Question extends React.Component {
    constructor(props) {
        super(props);
        this.question = props.question;
    }

    setAnswer = (answer) => {
        this.question.answer = answer;
        this.props.answerQuestion(this.question);
    };

    render() {
        const answers = this.question.answers.map(answer=>
            <Answer setAnswer={this.setAnswer} selected={this.question.answer && this.question.answer.id === answer.id}
                    key={answer.id} answer={answer}/>
        );
        return (
            <div className={'question'}>
                <h4>{this.question.text} {this.props.numbers}</h4>
                <div className={'answers-wrapper'}>
                    {answers}
                </div>
            </div>
        );
    }
}
