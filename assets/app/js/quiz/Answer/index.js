
import './style.scss';

import React from 'react';

export default class Answer extends React.Component {

    constructor(props) {
        super(props);
        this.answer = props.answer;
    }

    setAnswer = () => {
        const {answer, setAnswer} = this.props;
        setAnswer(answer);
    };

    render() {
        return (
            <div onClick={this.setAnswer} className={'answer' + (this.props.selected ? ' selected' : '')}>
                <span className="circle"/> <span>{this.answer.text}</span>
            </div>
        )
    }

}
