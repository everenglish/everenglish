import React, {Component} from "react";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Quiz from './Quiz';
import Result from './Result';

export default class App extends Component {
    constructor() {
        super();
    }
    state = {};

    displayResult = (result) => {
        this.setState({
            result: result
        })
    };

    removeResult = () => {
        this.setState({
            result: undefined
        })
    };

    render() {

        const state = this.state;
        let component;

        if (state.result) {
            component = <Result removeResult={this.removeResult} result={state.result}/>
        } else {
            component = <Quiz displayResult={this.displayResult} questions={this.state}/>
        }

        return (
            <div className={'quiz-wrapper'}>

                <h2>Тест рівня - Граматика</h2>
                <p>
                    Всього 40 питань. Ви отримаєте результати після того, як відповісте на всі
                    запитання. Деякі
                    питання легші, деякі складніші. Не хвилюйтесь, якщо не знаєте відповіді! Спробуйте НЕ
                    використовувати книги або інші веб-сайти під час тесту - ідея полягає у тому, щоб оцінити
                    свій дійсний рівень
                </p>

                <ReactCSSTransitionGroup
                    transitionName="reveal"
                    transitionEnterTimeout={0}
                    transitionLeaveTimeout={0}
                    component="div"
                >
                    {component}
                </ReactCSSTransitionGroup>
            </div>
        );
    }
}
