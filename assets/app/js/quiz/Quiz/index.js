import React, {Component} from "react";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import "./Quiz.scss";

import Question from '../Question';
import Result from '../Result';

export default class Quiz extends Component {
    constructor() {
        super();
    }

    state = {
        allow: false,
        index: 0
    };

    componentDidMount() {
        fetch( '/quiz/random').then(response => response.json()).then(
            (result) => {
                this.setState({
                    id: result.id,
                    loaded: true,
                    questions: result.questions,
                    answer: '',
                    currentQuestion: result.questions[0]
                });
            },
            (error) => {
                this.setState({
                    error: error
                })
            }
        );
    }

    answerQuestion = (question) => {
        this.setState(state => {
            const questions = state.questions.map((item) => {
                if (item.id === question.id) {
                    return question
                }
                return item
            });
            return {
                questions
            };
        }, () => {
            this.validate();
        });
    };

    validate = () => {
        const questions = this.state.questions;
        for (let i = 0; i < questions.length; i++) {
            if (!questions[i].answer) {
                return false;
            }
        }

        this.setState({
            allow: true
        });
        return true;
    };

    submit = () => {

        if (!this.validate()) {
            return;
        }
        let data = {
            id: this.state.id,
            questions: this.state.questions
        };
        fetch('/quiz/submit', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(response => response.json()).then(
            result => {
                this.props.displayResult(result);
            },
            error => {
                console.warn(error);
            }
        )

    };

    quickPass = () => {

        let passed = [];

        this.state.questions.forEach(question => {
            passed.push({
                id: question.id,
                answer: question.answers[Math.floor(Math.random() * 3)]
            })
        });
        this.setState({
            questions: passed
        }, () => {
            this.submit();
        })
    };

    prev = () => {
        let state = this.state;
        if (!state.questions[state.index - 1]) {
            return;
        }
        this.setState({
            currentQuestion: state.questions[state.index - 1],
            index: state.index - 1
        })
    };

    next = () => {
        let state = this.state;
        if (!state.questions[state.index + 1]) {
            return;
        }
        this.setState({
            currentQuestion: state.questions[state.index + 1],
            index: state.index + 1
        })
    };

    render() {

        const state = this.state;

        if (state.loaded) {

            if (state.result) {
                return <Result result={state.result}/>
            }

            return (
                <div className={'questions-list'}>
                    <ReactCSSTransitionGroup
                        className="questions"
                        transitionName="reveal"
                        transitionEnterTimeout={0}
                        transitionLeaveTimeout={0}
                        component="div"
                    >
                        <button className="button blue" style={{'width':'330px', 'margin':'auto'}} onClick={this.quickPass}>Quick pass</button>

                        <Question answerQuestion={this.answerQuestion}
                                  numbers={state.index + 1 + '/' + state.questions.length}
                                  question={state.currentQuestion}
                                  key={state.currentQuestion.id}/>
                    </ReactCSSTransitionGroup>
                    <div className="nav-bar">
                        <button disabled={state.index < 1}
                                onClick={this.prev}
                                className={'nav-button prev'}>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M8 16L9.41 14.59L3.83 9L16 9L16 7L3.83 7L9.41 1.41L8 -6.99382e-07L6.99382e-07 8L8 16Z"
                                    fill="#0093B9"/>
                            </svg>
                        </button>
                        <button disabled={state.allow === false}
                                onClick={this.submit}
                                className={'nav-button submit'}>
                            <span>
                                Відправити
                            </span>
                        </button>
                        <button disabled={state.index >= state.questions.length - 1}
                                onClick={this.next}
                                className={'nav-button next'}>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M8 16L9.41 14.59L3.83 9L16 9L16 7L3.83 7L9.41 1.41L8 -6.99382e-07L6.99382e-07 8L8 16Z"
                                    fill="#0093B9"/>
                            </svg>
                        </button>
                    </div>
                </div>
            );
        }

        return (
            <div className={'questions-wrapper'}>
                Waiting
            </div>
        );

    }
}
