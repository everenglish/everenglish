import '../../../node_modules/materialize-css/dist/js/materialize.min.js';

export default class Materialize {

    constructor() {
        this.start();
    }

    start() {
        let formErrors = document.querySelector('.form-errors');
        let tabs = document.querySelectorAll('.nav-tabs');
        let collapsible = document.querySelector('.collapsible.expandable');
        let elems = document.querySelectorAll('.carousel');
        let options = {
            numVisible: 5,
            fullWidth: true,
            indicators: true
        };
        M.Carousel.init(elems, options);

        M.AutoInit();
        M.Tabs.init(tabs, {
            swipeable: true,
            duration: 100
        });
        M.Collapsible.init(collapsible, {
            accordion: false
        });

        if (formErrors !== null) {
            let toastHTML = formErrors.innerHTML;
            M.toast({html: toastHTML});
        }
    }

}
