
import '../scss/style.scss';

import MultipleForms from './utils/MultipleForms';
import DeleteRequests from './utils/DeleteRequest';
import Materialize from './Materialize';

class App {
    run(){
        let fileUpload = new MultipleForms();
        let requests = new DeleteRequests();
        let materialize = new Materialize();
    }

}

document.addEventListener('DOMContentLoaded', function () {
    const app = new App();
    app.run();
});