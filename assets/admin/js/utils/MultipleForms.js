export default class MultipleForms {
    constructor() {
        this.addButton = document.querySelector('.add-record');
        this.formList = document.querySelector('#multiple-list');
        this.removables = document.querySelectorAll('.removable');
        if(this.addButton !== null && this.formList !== null){
            this.init();
        }
    }

    init() {
        this.addInputs();
        this.removeInputs();
    }

    addInputs(){
        this.addButton.addEventListener('click', () => {
            let counter = this.formList.childElementCount;
            let innerMarkup = this.formList.dataset.prototype;
            innerMarkup = innerMarkup.replace(/__name__/g, counter);

            let inputBlock = document.createElement('DIV');
            inputBlock.classList.add('removable', 'col', 'm4', 's6');
            inputBlock.innerHTML = '<div><button class="waves-effect waves-light btn-small orange darken-1 delete-form">Удалить</button>'
                + innerMarkup + '</div>';

            if(innerMarkup.includes(`appbundle_question[answers][${counter}][text]`)){
                let targetLabel = inputBlock.querySelector(`label[for="appbundle_question_answers_${counter}_valid"]`);
                let fragment = document.createDocumentFragment();
                targetLabel.innerHTML = '<span>Правильный</span>';
                inputBlock.querySelector(`#appbundle_question_answers_${counter}_text`).required = true;
                fragment.appendChild(inputBlock.querySelector(`#appbundle_question_answers_${counter}_valid`));
                targetLabel.prepend(fragment);
            }

            this.formList.prepend(inputBlock);
            this.removables = document.querySelectorAll('.removable');
            this.removeInputs(this.removables);
        })
    }

    removeInputs(){
        let self = this;
        this.removables.forEach(element=>{
            element.addEventListener('click', function (e) {
                if(e.target.classList.contains('delete-form')){
                    element.parentNode.removeChild(element);
                }
                if(e.target.type === 'checkbox'){
                    self.formList.querySelectorAll('input[type="checkbox"]').forEach(input => {
                        input.checked = false;
                    });
                    e.target.checked = !e.target.checked
                }
            })
        })
    }

}

