
import axios from "axios";

export default class DeleteRequest {
    constructor() {
        this.modal = document.querySelectorAll('.modal');
        this.modal_links = document.getElementsByClassName('modal-trigger');
        this.confirm_button = document.getElementById('confirmDelete');
        this.delete_url = '';
        this.id_prefix = '#record-';
        this.materialize_modal = M.Modal.init(this.modal);
        this.init();
    }

    init() {
        let self = this;
        for (let i = 0; i < this.modal_links.length; i++) {
            this.modal_links[i].addEventListener('click', function (e) {
                e.preventDefault();
                self.delete_url = this.dataset.url;
            })
        }

        this.confirmDelete(self);
    }

    confirmDelete(self){
        self.confirm_button.addEventListener('click', function () {
            axios.delete(self.delete_url).then(function () {
                let selector = self.id_prefix + self.delete_url.split('/').pop();
                let element = document.querySelector(selector);
                element.remove();
                M.toast({html: 'Запись удалена!'})
            });
        });
    }

}
