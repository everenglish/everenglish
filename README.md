
# Everenglish

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes. See **Deployment** for notes on how to deploy the project on a live system. Current version (05.08.2020) of the application has
basic admin panel functionality;

### Environment

You will need a running server (Apache or Nginx) with these modules in order to run the application: 
   *  PHP >= 7.2
   *  MySQL >= 5.7

### Installing:

A step by step series of examples that will tell you how to get a development environment and application running.
Clone the app from repository running in your terminal:
   
```
git clone https://gitlab.com/everenglish/everenglish.git
```
   #### Back-end
  
  In order to set up back-end of the application, you will need to have a database, where application 
  will store different data. 
  
  ***First step:*** create a new database, you will need to know following to go through next steps:
    
  * Database name
  * Database user
  * Database user password
  * Database host

  ***Second step:*** After you created a database, run in console:
  
```
php composer.phar install
```
or if you have it installed globally:

```
composer install
```

  While installing dependencies, Composer will ask you to enter your database credentials
   
   If something went wrong while entering database credentials, or while installing dependencies console didn't prompt 
   you for DB credentials don't worry, you can always edit file
   `app/config/parameters.yml` or remove `.dist` prefix from `parameters.yml.dist` in the same folder.
   Fill out the corresponding fields, they might look something like this, you should replace curly braces and their
   content with your credentials 
   ```
       database_host: {{database host}}
       database_name: {{database name}}
       database_user: {{database user}}
       database_password: {{database password}}
   ```
   
  ***Third step:*** You will need to import the `everenglish.sql.gz` into your database, to have a ready quiz, 
  questions and answers in a DB and served to front-end part of project. If you're using phpMyAdmin - navigate to your database,
  then on top, click **Import** link, upload the file to form and press **Go** at the bottom of the page

 
  ***Fourth step:*** After installing all backend dependencies, make sure, that database structure and server-side code will work properly.
  
  **After you imported** `everenglish.sql.gz` - run the following command:
      
    php bin/console doctrine:migrations:migrate

  ***Fifth step:*** Run this command to open project in browser (usually http://127.0.0.1:8000):
    
    php bin/console server:start

   #### Front-end
All source files for front-end part of project are located in `asstes` directory, folders are named according to their purpose

To install front-end dependencies run in terminal
```
npm install
```

Now you may run following command to start a dev-server for front-end part:

```
npm run dev-server
```
At this point you may go to `/admin/register` path to create admin user, to be able to create/edit/delete quiz related data

## Deployment

Assuming, that this project would be deployed to shared hosting, please, make sure, that web root of your hosting account
 would point to `web` directory of the project
 
Before deploying to production server you will need to run `npm run build` to create a new build
of static files like .js and .css, required for client side
 
## Built With

* [Symfony](https://symfony.com/doc/current/index.html#gsc.tab=0) - PHP framework, version 3.4
* [Composer](https://getcomposer.org/) - PHP bundles dependency manager
* [React](https://reactjs.org/) - A JavaScript library for building user interfaces
* [NPM](https://nodejs.org/) - Front-end dependency manager
* [Webpack](https://webpack.js.org/) - Front-end module bundler
* [Webpack Encore](https://www.npmjs.com/package/@symfony/webpack-encore) - Webpack integration for Symfony

