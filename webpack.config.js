const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/dist')
    .setPublicPath('/dist')
    .addEntry('app', './assets/app/js/app.js')
    .addEntry('admin', './assets/admin/js/app.js')
    // TODO: Create here separate styles for login pages, no need to import all styles of admin or app bundles
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .configureBabel(() => {
    }, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .enableSassLoader()
    .enablePostCssLoader()
    .addPlugin(new CopyWebpackPlugin([
        {from: './assets/fonts', to: './fonts'},
        {from: './assets/images', to: './images'}
    ]))
    .addPlugin(new ImageminPlugin({
        test: /\.(jpe?g|png|gif|svg)$/i
    }))
;
const config = Encore.getWebpackConfig();

config.devServer = {
    disableHostCheck: true,
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
};

module.exports = config;