#!/usr/bin/env bash

# db cache
php bin/console doctrine:cache:clear-metadata
php bin/console doctrine:cache:clear-query
php bin/console doctrine:cache:clear-result

# dev
php bin/console cache:clear -e dev

# prod
php bin/console cache:clear -e prod --no-debug
