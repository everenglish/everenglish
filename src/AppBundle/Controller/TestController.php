<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquiry;
use AppBundle\Entity\Quiz;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends Controller
{
    /**
     * @Route("/quiz/random", name="get_quiz", methods={"GET"})
     */
    public function getQuizAction()
    {
        $quizes = $this->getDoctrine()->getRepository(Quiz::class)->findAll();
        $rand_keys = array_rand($quizes, 1);
        $quiz = $quizes[$rand_keys];

        $questions = $quiz->getQuestions()->toArray();
        shuffle($questions);
        foreach ($questions as $question) {
            $answers = $question->getAnswers()->toArray();
            shuffle($answers);
        }

        $json_data = [
            'id' => $quiz->getId(),
            'name' => $quiz->getName(),
            'questions' => $questions
        ];

        $jsonContent = $this->container->get('jms_serializer')->serialize($json_data, 'json');
        return new Response($jsonContent, 200);
    }

    /**
     * @Route("/quiz/submit", name="submit_quiz", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function submitQuizAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $quiz = $this->getDoctrine()->getRepository(Quiz::class)->findOneBy(['id' => $data['id']]);
        $result = $this->getDoctrine()->getManager()->getRepository(Quiz::class)->compareAnswers($data, $quiz);

        return new Response(json_encode([
            'result' => $result
        ]), 200);
    }

    /**
     * @Route("/submit", name="submit", methods={"POST","GET"})
     */
    public function submitAction(Request $request)
    {
        $enquiry = new Enquiry();
        $form = $this->createForm('AppBundle\Form\EnquiryType', $enquiry);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $enquiry->setDate(new \DateTime());
            $em->persist($enquiry);
            $em->flush();
            try {
                $this->sendNotification($enquiry);
            } catch (\Exception $exception) {
                return new Response(json_encode($exception), 422);
            }
            return new Response('', 200);
        }
        return new Response('', 422);
    }

    /**
     * @param $enquiry
     */
    public function sendNotification($enquiry)
    {
        $message = (new \Swift_Message('Ever English - New Enquiry'))
            ->setFrom('noreply@api.ever-english.com', 'Message Bot')
            ->setTo('yana.ever.english@gmail.com')
            ->setPriority(1)
            ->setBody(
                $this->renderView(
                    '@Api/emails/enquiry-notification.html.twig',
                    [
                        'enquiry' => $enquiry
                    ]
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }
}
