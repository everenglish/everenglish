<?php
/**
 * Created by PhpStorm.
 * User: jeka
 * Date: 14.01.2019
 * Time: 18:02
 */
namespace AdminBundle\Service;

use AppBundle\Entity\Icon;

class IconsService
{
    public function renderIcons($icon)
    {
        $android_sizes = [16,32,72,96,144,192];
        $apple_sizes = [57,60,72,76,114,120,144,152,180];

        foreach ($android_sizes as $android_size){
            dump('android-icon-'.$android_size.'x'.$android_size.'.png');
        }

        foreach ($apple_sizes as $apple_size){
            dump('apple-icon-'.$apple_size.'x'.$apple_size.'.png');
        }

        $icons = [];

        return $icons;
    }
}