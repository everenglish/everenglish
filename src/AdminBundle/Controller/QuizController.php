<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Quiz;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Quiz controller.
 *
 * @Route("quiz", name="admin_")
 */
class QuizController extends Controller
{
    /**
     * Lists all quiz entities.
     *
     * @Route("/", name="quiz_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $quizzes = $em->getRepository('AppBundle:Quiz')->findAll();

        return $this->render('@Admin/quiz/index.html.twig', array(
            'quizzes' => $quizzes,
        ));
    }

    /**
     * Creates a new quiz entity.
     *
     * @Route("/new", name="quiz_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $quiz = new Quiz();
        $form = $this->createForm('AppBundle\Form\QuizType', $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($quiz);
            $em->flush();

            return $this->redirectToRoute('admin_quiz_edit', array('id' => $quiz->getId()));
        }

        return $this->render('@Admin/quiz/new.html.twig', array(
            'quiz' => $quiz,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a quiz entity.
     *
     * @Route("/{id}", name="quiz_show")
     * @Method("GET")
     */
    public function showAction(Quiz $quiz)
    {

        return $this->render('@Admin/quiz/show.html.twig', array(
            'quiz' => $quiz,
        ));
    }

    /**
     * Displays a form to edit an existing quiz entity.
     *
     * @Route("/{id}/edit", name="quiz_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Quiz $quiz)
    {

        $originalQuestions = new ArrayCollection();

        foreach ($quiz->getQuestions() as $question) {
            $originalQuestions->add($question);
        }

        $editForm = $this->createForm('AppBundle\Form\QuizType', $quiz);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            foreach ($originalQuestions as $question) {
                if (false === $quiz->getQuestions()->contains($question)) {
                    $question->setQuiz(null);
                    $this->getDoctrine()->getManager()->remove($question);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_quiz_edit', array('id' => $quiz->getId()));
        }

        return $this->render('@Admin/quiz/edit.html.twig', array(
            'quiz' => $quiz,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a quiz entity.
     *
     * @Route("/delete/{id}", name="quiz_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Quiz $quiz)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($quiz);
        $em->flush();

        return new Response('Deleted', 200);

    }
}
