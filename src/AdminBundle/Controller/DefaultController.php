<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Manifest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_home", methods={"GET"})
     */
    public function indexAction()
    {
        $enquiries = $this->getDoctrine()->getRepository('AppBundle:Enquiry')->findBy(array(), array('date' => 'DESC'), 4);
        return $this->render('@Admin/default/index.html.twig',[
            'enquiries' => $enquiries,
        ]);
    }

}
