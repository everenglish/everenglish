<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\AdminUser;
use AdminBundle\Form\AdminUserType;
use AdminBundle\Form\ChangePasswordType;
use AdminBundle\Form\ForgottenUserType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="admin_login", methods={"GET","POST"})
     */
    public function loginAction()
    {
        $user = $this->getUser();
        if ( $user !== null && $user->getRoles()[0] === 'ROLE_ADMIN' ){
            return $this->redirectToRoute('admin_home');
        } else if ($user !== null && $user->getRoles()[0] === 'ROLE_AUTHOR'){
            return $this->redirectToRoute('post_index');
        }
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('@Admin/security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/login-check", name="admin_login_success", methods={"GET"})
     */
    public function loginSuccessAction()
    {
        $user = $this->getUser();
        if ($user !== null && $user->getRoles()[0] === 'ROLE_AUTHOR'){
            return $this->redirectToRoute('post_index');
        }
        return $this->redirectToRoute('admin_home');
    }

    /**
     * @Route("/logout", name="admin_logout")
     */
    public function logoutAction()
    {

    }

    /**
     * @Route("/register", name="admin_registration", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerAction(Request $request)
    {
        $existing_user = $this->getDoctrine()->getRepository(AdminUser::class)->findAll();
        if ( count($existing_user) > 0 ){
            return $this->redirectToRoute('admin_home');
        }
        $user = new AdminUser();
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('admin_login');
        }
        return $this->render(
            '@Admin/security/register.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/lost-password", name="admin_lost_password", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function lostAction(Request $request)
    {
        $tmpUser = new AdminUser();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ForgottenUserType::class, $tmpUser);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $em->getRepository(AdminUser::class)->findOneBy(['email' => $tmpUser->getUsername()]);
            if ($user) {
                if ($user->getConfirmToken() === null) {
                    $token = md5(uniqid($user->getUsername(), true));
                    $user->setConfirmToken($token);
                    $em->persist($user);
                    $em->flush();
                    $message = (new \Swift_Message('Восстановление пароля'))
                        ->setFrom('noreply@development.themoon.agency')
                        ->setTo($user->getEmail())
                        ->setBody(
                            $this->renderView(
                                '@Admin/emails/restore-password.html.twig',
                                [
                                    'url' => $this->generateUrl('admin_new_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL),
                                    'user' => $user
                                ]
                            ),
                            'text/html'
                        )
                    ;
                    $this->get('mailer')->send($message);
                    return new RedirectResponse($this->generateUrl('admin_login'));
                }
                return new RedirectResponse($this->generateUrl('admin_login'));
            }
        }
        return $this->render("@Admin/security/forgot-password.html.twig", array(
            "form" => $form->createView()
        ));
    }

    /**
     * Reset user password.
     * @Route("/new-password/{token}", name="admin_new_password", methods={"GET","POST"})
     * @param $token
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function resetAction($token ,Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(AdminUser::class)->findOneBy(['confirmToken' => $token]);
        if (null === $user) {
            return new RedirectResponse($this->generateUrl('admin_lost_password'));
        }
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->SetConfirmToken(null);
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $entityManager->persist($user);
            $entityManager->flush();
            $response = new RedirectResponse($this->generateUrl('admin_login'));
            return $response;
        }
        return $this->render('@Admin/security/new-password.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
